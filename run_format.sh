#!/bin/bash


# Excluding both third_party and build folders, clang-format with either of the C++ related extensions.
# Using -print to only show files and not matching directories.
echo "---------- running clang-format ----------"
find . -type d \( -path ./.cache -o -path ./.git -o -path ./third_party -o -path ./build -o -path ./models \) -prune -o \( -name '*.h' -o -name '*.hh' -o -name '*.cpp' -o -name '*.cc' \) -print | xargs clang-format -i


echo "---------- running dos2unix ----------"
# CRLF -> LF
find . -type d \( -path ./third_party -o -path ./build -o -path ./.git \) -prune -o -name '*.*' -print0 | xargs -0 dos2unix -q

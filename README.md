## cpp-snippets
This repo contains C++ snippets for and from various things.



## Build and Requirements

Just run `./run_build.sh`, CMake should fetch the dependencies

Once the project is setup, you can use `./run_rebuild.sh exe_folder_file` (replacing `exe_folder_file` with the snippet folder/file) to build only that snippet and run it.

Dependencies:
- Google Benchmark
- Google Test



## Structure
The basic premise here is that all the snippets build standalone
- `./src/util`, `./src/test`, and `./src/benchmark/` are not considered snippets
- `./src/util/` is linked throughout the project
- Snippets are built such that: `./src/{folder}/{filename}.cc` --> `./build/exe_{folder}_{filename}`
- - For example, `./src/leetcode/1_two_sum.cc` -> `./build/exe_leetcode_1_two_sum`
- `main.cc` --> `./build/cpp-snippets`. This is just a placeholder exe that executes after the build shell scripts.

Each snippet must define an `auto main() -> int {}` entrypoint

If there are corresponding tests or benchmarks for the snippets:
- In the snippet, use `#ifndef SNIPPET_ENTRYPOINT_SET` to set the `main()` function.
- In the test/benchmark, `#define SNIPPET_ENTRYPOINT_SET` _before_ `#include`'ing the snippet


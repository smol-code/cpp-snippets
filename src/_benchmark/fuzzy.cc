#include <benchmark/benchmark.h>

#define SNIPPET_ENTRYPOINT_SET
#include "../mystuffhere/fuzzy.cc"



// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

void bench_mystuffhere_fuzy_basic(benchmark::State &state)
{
	for (auto nop : state) {
		std::vector<int> vec_1 { 2, 7, 11, 15 };
		// twoSum1(vec_1, 9);
	}
}

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

BENCHMARK(bench_mystuffhere_fuzy_basic);

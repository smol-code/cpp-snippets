#include <cstdlib>
#include <iostream>
#include <string>
#include <thread>
#include <vector>


// This downloads all the files on an auto-indexed HTTP server...
// on multiple threads at the same time, to serve as a light synthetic
// load against atatic file servers (e.g., NGINX).

const std::string server_url = "https://asdf";

auto run_wget(int count) -> void
{
	std::string cmd { "wget -q -r -np -nH --cut-dirs=3 -R index.html " };
	cmd += server_url + " -P ./out_";
	cmd += std::to_string(count) + "lol";
	std::cout << "Executing: " << cmd << "\n";
	system(cmd.c_str());
}

auto main() -> int
{

	std::vector<std::thread> threads;
	const int thread_count = 100;

	int count = 0;

	while (count != thread_count) {
		threads.emplace_back(run_wget, count);
		count++;
	}

	count = 0;
	for (auto &thr : threads) {
		std::cout << "Joining thread: " << count << "\n";
		thr.join();
		count++;
	}
}

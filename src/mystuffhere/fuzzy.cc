

#include <cstdio>
#include <cstdlib>
int* levenshtein() {
	int* ret = (int*)malloc(100 * sizeof( int));
	ret[0] = 1;
	ret[1] = 2;
	return ret;
}

#ifndef SNIPPET_ENTRYPOINT_SET
#define SNIPPET_ENTRYPOINT_SET
int main() {
	int* arr = levenshtein();
	// std::cout << arr[0] << " " << arr[1] << " " << arr[2] << '\n';
	printf("num: %d", arr[0]);
	free(arr);
}
#endif

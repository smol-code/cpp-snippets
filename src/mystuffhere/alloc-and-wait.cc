#include <cstdint>
#include <cstring>
#include <iostream>



auto main(int argc, char *argv[]) -> int
{
	int32_t alloc_mebibyte { 1024 };
	if (argc == 2) {
		alloc_mebibyte = std::stoi(argv[1]);
		std::cout << "- Will allocate " << alloc_mebibyte << "MiB" << '\n';
	}

	std::cout << '\n';
	std::cout << "- This executable will allocate the provided amount of MiB and hold onto it." << '\n';
	std::cout << "- It defaults to 1024MiB if no arg provided." << '\n';

	// char *mem = static_cast<char *>(malloc(1024 * 1024 * alloc_mebibyte * 1000));
	for (int32_t i = 0; i != alloc_mebibyte; ++i) {
		char *mem1 = new char[1024 * 1024];

		for (int32_t j = 0; j != 1024 * 1024; ++j) {
			mem1[j] = j;
		}
	}

	do {
		std::cout << "Enter \"q\" key to exit..." << '\n';
	} while (std::cin.get() != 'q');

	// I should free here but I won't :3
	return 0;
}

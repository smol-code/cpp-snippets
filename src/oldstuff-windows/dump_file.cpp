// NOLINTBEGIN

// IO
#include <iostream>
// File
#include <fstream>
// Performance
#include <chrono>
// Types
#include <string>
#include <vector>

std::ofstream dumpFile;

void writeToFile(int a)
{
    dumpFile << a + '\n';
}

void makeRandoms()
{
    std::vector<long long> allRandoms;
    for (int i = 0; i < 10000000; i++)
    {
        long long a;
        a = rand() % 9223372036854775666;
        // allRandoms.push_back(a);
        writeToFile(a);
    }
}

int main()
{
    dumpFile.open("example.txt", std::ios::out);

    std::cout << "start\n";

    auto t1 = std::chrono::high_resolution_clock::now();
    makeRandoms();
    auto t2 = std::chrono::high_resolution_clock::now();
    auto duration = std::chrono::duration_cast<std::chrono::microseconds>(t2 - t1).count();

    std::cout << "Writing done\n";

    std::cout << "Time to execute: " << duration << '\n';

    std::cin.ignore();
    std::string exitInput;
    std::cin >> exitInput;

    dumpFile.close();
    return 0;
}

// NOLINTEND

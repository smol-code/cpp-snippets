// NOLINTBEGIN

// IO
#include <iostream>
// File
#include <fstream>
// Performance
#include <chrono>
// Types
#include <string>
#include <vector>

int main()
{
    int loopCount = 100000000;
    std::vector<int> loopOutput;

    for (int i = 0; i < loopCount; i++)
    {
        // Random number
        long long int q = rand() % 9223372036854775666;

        // std::cout << q << std::endl;

        // Push into vector
        loopOutput.push_back(q);
    }
    system("pause");

    for (auto i = loopOutput.begin(); i != loopOutput.end(); ++i)
        std::cout << *i << ' ';

    system("pause");

    return 0;
}

// NOLINTEND

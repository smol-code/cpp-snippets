#include <gtest/gtest.h>
#include <vector>

#define SNIPPET_ENTRYPOINT_SET
#include "../leetcode/4_median_two_sorted_arr.cc"


// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

// Implementation #1
TEST(leetcode, _4_median_two_sorted_arr__impl1_case1)
{
	std::vector<int> input_1 { 1, 3 };
	std::vector<int> input_2 { 2 };
	EXPECT_EQ(findMedianSortedArrays1(input_1, input_2), 2);
}

TEST(leetcode, _4_median_two_sorted_arr__impl1_case2)
{
	std::vector<int> input_1 { 1, 2 };
	std::vector<int> input_2 { 3, 4 };
	EXPECT_EQ(findMedianSortedArrays1(input_1, input_2), 2.5);
}

TEST(leetcode, _4_median_two_sorted_arr__impl1_case3)
{
	std::vector<int> input_1 { 1, 3 };
	std::vector<int> input_2 { 2, 7 };
	EXPECT_EQ(findMedianSortedArrays1(input_1, input_2), 2.5);
}

TEST(leetcode, _4_median_two_sorted_arr__impl1_case4)
{
	std::vector<int> input_1 { 1 };
	std::vector<int> input_2 { 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	EXPECT_EQ(findMedianSortedArrays1(input_1, input_2), 5.5);
}


// Implementation #2
TEST(leetcode, _4_median_two_sorted_arr__impl2_case1)
{
	std::vector<int> input_1 { 1, 3 };
	std::vector<int> input_2 { 2 };
	EXPECT_EQ(findMedianSortedArrays2(input_1, input_2), 2);
}

TEST(leetcode, _4_median_two_sorted_arr__impl2_case2)
{
	std::vector<int> input_1 { 1, 2 };
	std::vector<int> input_2 { 3, 4 };
	EXPECT_EQ(findMedianSortedArrays2(input_1, input_2), 2.5);
}

TEST(leetcode, _4_median_two_sorted_arr__impl2_case3)
{
	std::vector<int> input_1 { 1, 3 };
	std::vector<int> input_2 { 2, 7 };
	EXPECT_EQ(findMedianSortedArrays2(input_1, input_2), 2.5);
}

TEST(leetcode, _4_median_two_sorted_arr__impl2_case4)
{
	std::vector<int> input_1 { 1 };
	std::vector<int> input_2 { 2, 3, 4, 5, 6, 7, 8, 9, 10 };
	EXPECT_EQ(findMedianSortedArrays2(input_1, input_2), 5.5);
}

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

#include <gtest/gtest.h>
#include <vector>

#define SNIPPET_ENTRYPOINT_SET
#include "../leetcode/3_longest_substr_norepeat.cc"



// Implementation #1
TEST(leetcode, _3_longest_substr_norepeat_case1) { EXPECT_EQ(lengthOfLongestSubstring1("abcabcbb"), 3); }
TEST(leetcode, _3_longest_substr_norepeat_case2) { EXPECT_EQ(lengthOfLongestSubstring1("bbbbb"), 1); }
TEST(leetcode, _3_longest_substr_norepeat_case3) { EXPECT_EQ(lengthOfLongestSubstring1("pwwkew"), 3); }
TEST(leetcode, _3_longest_substr_norepeat_case4) { EXPECT_EQ(lengthOfLongestSubstring1("ynyo"), 3); }
TEST(leetcode, _3_longest_substr_norepeat_case5) { EXPECT_EQ(lengthOfLongestSubstring1(" "), 1); }

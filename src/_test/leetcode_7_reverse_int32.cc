#include <gtest/gtest.h>

#define SNIPPET_ENTRYPOINT_SET
#include "../leetcode/7_reverse_int32.cc"



// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

// Implementation #1
TEST(leetcode, _7_reverse_int32_case1)
{
	std::int32_t input_1 { 321 };
	EXPECT_EQ(reverse(input_1), 123);
}

TEST(leetcode, _7_reverse_int32_case2)
{
	std::int32_t input_1 { 123 };
	EXPECT_EQ(reverse(input_1), 321);
}

TEST(leetcode, _7_reverse_int32_case3)
{
	std::int32_t input_1 { -123 };
	EXPECT_EQ(reverse(input_1), -321);
}

TEST(leetcode, _7_reverse_int32_case4)
{
	std::int32_t input_1 { 120 };
	EXPECT_EQ(reverse(input_1), 21);
}

TEST(leetcode, _7_reverse_int32_case5)
{
	std::int32_t input_1 { 2147483647 };
	EXPECT_EQ(reverse(input_1), 0);
}

TEST(leetcode, _7_reverse_int32_case6)
{
	std::int32_t input_1 { -2147483648 };
	EXPECT_EQ(reverse(input_1), 0);
}

TEST(leetcode, _7_reverse_int32_case7)
{
	std::int32_t input_1 { 2000000009 };
	EXPECT_EQ(reverse(input_1), 0);
}

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

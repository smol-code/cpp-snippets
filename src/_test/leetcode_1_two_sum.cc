#include <gtest/gtest.h>
#include <vector>

#define SNIPPET_ENTRYPOINT_SET
#include "../leetcode/1_two_sum.cc"



// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

TEST(leetcode, _1_two_sum)
{
	std::vector<int> vec_1 { 2, 7, 11, 15 };
	std::vector<int> vec_1_out { 0, 1 };
	EXPECT_EQ(twoSum1(vec_1, 9), vec_1_out);
}

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

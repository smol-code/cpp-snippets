#include <gtest/gtest.h>

#define SNIPPET_ENTRYPOINT_SET
#include "../leetcode/6_zigzag.cc"



// Implementation #1
TEST(leetcode, _6_zigzag_case1)
{
	std::string input_1 { "PAYPALISHIRING" };
	std::string res = convert(input_1, 3);
	EXPECT_EQ(res, "PAHNAPLSIIGYIR");
}

TEST(leetcode, _6_zigzag_case2)
{
	std::string input_1 { "PAYPALISHIRING" };
	std::string res = convert(input_1, 4);
	EXPECT_EQ(res, "PINALSIGYAHRPI");
}

TEST(leetcode, _6_zigzag_case3)
{
	std::string input_1 { "A" };
	std::string res = convert(input_1, 1);
	EXPECT_EQ(res, "A");
}

#include <gmock/gmock.h>
#include <gtest/gtest.h>

#define SNIPPET_ENTRYPOINT_SET
#include "../leetcode/5_longest_palindrome_substr.cc"



// Implementation #1
TEST(leetcode, _5_longest_palindrome_substr_case1)
{
	std::string input_1 { "babad" };
	// Uses gmock
	EXPECT_THAT(longestPalindrome(input_1), testing::AnyOf("aba", "bab"));
}

TEST(leetcode, _5_longest_palindrome_substr_case2)
{
	std::string input_1 { "cbbd" };
	EXPECT_EQ(longestPalindrome(input_1), "bb");
}

TEST(leetcode, _5_longest_palindrome_substr_case3)
{
	std::string input_1 { "qqasdfdsagg" };
	EXPECT_EQ(longestPalindrome(input_1), "asdfdsa");
}

TEST(leetcode, _5_longest_palindrome_substr_case4)
{
	std::string input_1 { "aergbn oasdttdsao nbaaaaa" };
	EXPECT_EQ(longestPalindrome(input_1), "bn oasdttdsao nb");
}

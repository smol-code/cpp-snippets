#include <gtest/gtest.h>
#include <vector>

#define SNIPPET_ENTRYPOINT_SET
#include "../leetcode/2_add_two_numbers.cc"



// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

// Implementation #1
TEST(leetcode, _2_add_two_numbers__impl1_case1)
{
	ListNode input_1(2);
	input_1.next = new ListNode(4);
	input_1.next->next = new ListNode(3);

	ListNode input_2(5);
	input_2.next = new ListNode(6);
	input_2.next->next = new ListNode(4);

	ListNode *res_actual = addTwoNumbers1(&input_1, &input_2);

	EXPECT_EQ(res_actual->val, 7);
	EXPECT_EQ(res_actual->next->val, 0);
	EXPECT_EQ(res_actual->next->next->val, 8);
	input_1.naive_destructor();
	input_2.naive_destructor();
	res_actual->naive_destructor();
	delete res_actual;
}

TEST(leetcode, _2_add_two_numbers__impl1_case2)
{
	ListNode input_1(0);

	ListNode input_2(0);

	ListNode *res_actual = addTwoNumbers1(&input_1, &input_2);

	EXPECT_EQ(res_actual->val, 0);
	EXPECT_EQ(res_actual->next, nullptr);
	input_1.naive_destructor();
	input_2.naive_destructor();
	res_actual->naive_destructor();
	delete res_actual;
}

TEST(leetcode, _2_add_two_numbers__impl1_case3)
{
	// [9,9,9,9,9,9,9]
	ListNode input_1(9);
	input_1.next = new ListNode(9);
	input_1.next->next = new ListNode(9);
	input_1.next->next->next = new ListNode(9);
	input_1.next->next->next->next = new ListNode(9);
	input_1.next->next->next->next->next = new ListNode(9);
	input_1.next->next->next->next->next->next = new ListNode(9);

	// [9,9,9]
	ListNode input_2(9);
	input_2.next = new ListNode(9);
	input_2.next->next = new ListNode(9);
	input_2.next->next->next = new ListNode(9);

	ListNode *res_actual = addTwoNumbers1(&input_1, &input_2);

	// [8,9,9,9,0,0,0,1]
	EXPECT_EQ(res_actual->val, 8);
	EXPECT_EQ(res_actual->next->val, 9);
	EXPECT_EQ(res_actual->next->next->val, 9);
	EXPECT_EQ(res_actual->next->next->next->val, 9);
	EXPECT_EQ(res_actual->next->next->next->next->val, 0);
	EXPECT_EQ(res_actual->next->next->next->next->next->val, 0);
	EXPECT_EQ(res_actual->next->next->next->next->next->next->val, 0);
	EXPECT_EQ(res_actual->next->next->next->next->next->next->next->val, 1);
	input_1.naive_destructor();
	input_2.naive_destructor();
	res_actual->naive_destructor();
	delete res_actual;
}

// Implementation #2
TEST(leetcode, _2_add_two_numbers__impl2_case1)
{
	ListNode input_1(2);
	input_1.next = new ListNode(4);
	input_1.next->next = new ListNode(3);

	ListNode input_2(5);
	input_2.next = new ListNode(6);
	input_2.next->next = new ListNode(4);

	ListNode *res_actual = addTwoNumbers2(&input_1, &input_2);

	EXPECT_EQ(res_actual->val, 7);
	EXPECT_EQ(res_actual->next->val, 0);
	EXPECT_EQ(res_actual->next->next->val, 8);
	input_1.naive_destructor();
	input_2.naive_destructor();
	res_actual->naive_destructor();
	delete res_actual;
}

TEST(leetcode, _2_add_two_numbers__impl2_case2)
{
	ListNode input_1(0);

	ListNode input_2(0);

	ListNode *res_actual = addTwoNumbers2(&input_1, &input_2);

	EXPECT_EQ(res_actual->val, 0);
	EXPECT_EQ(res_actual->next, nullptr);
	input_1.naive_destructor();
	input_2.naive_destructor();
	res_actual->naive_destructor();
	delete res_actual;
}

TEST(leetcode, _2_add_two_numbers__impl2_case3)
{
	// [9,9,9,9,9,9,9]
	ListNode input_1(9);
	input_1.next = new ListNode(9);
	input_1.next->next = new ListNode(9);
	input_1.next->next->next = new ListNode(9);
	input_1.next->next->next->next = new ListNode(9);
	input_1.next->next->next->next->next = new ListNode(9);
	input_1.next->next->next->next->next->next = new ListNode(9);

	// [9,9,9]
	ListNode input_2(9);
	input_2.next = new ListNode(9);
	input_2.next->next = new ListNode(9);
	input_2.next->next->next = new ListNode(9);

	ListNode *res_actual = addTwoNumbers2(&input_1, &input_2);

	// [8,9,9,9,0,0,0,1]
	EXPECT_EQ(res_actual->val, 8);
	EXPECT_EQ(res_actual->next->val, 9);
	EXPECT_EQ(res_actual->next->next->val, 9);
	EXPECT_EQ(res_actual->next->next->next->val, 9);
	EXPECT_EQ(res_actual->next->next->next->next->val, 0);
	EXPECT_EQ(res_actual->next->next->next->next->next->val, 0);
	EXPECT_EQ(res_actual->next->next->next->next->next->next->val, 0);
	EXPECT_EQ(res_actual->next->next->next->next->next->next->next->val, 1);
	input_1.naive_destructor();
	input_2.naive_destructor();
	res_actual->naive_destructor();
	delete res_actual;
}

// NOLINTEND(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)

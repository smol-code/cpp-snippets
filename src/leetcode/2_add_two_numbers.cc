#include "../util/snippet_location.hh"
#include <map>



// Leetcode:
// You are given two non-empty linked lists representing two non-negative integers. The digits are stored in reverse
// order, and each of their nodes contains a single digit. Add the two numbers and return the sum as a linked list.
//
// You may assume the two numbers do not contain any leading zero, except the number 0 itself.
//
// Example 1:
// Input: l1 = [2,4,3], l2 = [5,6,4]
// Output: [7,0,8]
// Explanation: 342 + 465 = 807.
//
// Example 2:
// Input: l1 = [0], l2 = [0]
// Output: [0]
//
// Example 3:
// Input: l1 = [9,9,9,9,9,9,9], l2 = [9,9,9,9]
// Output: [8,9,9,9,0,0,0,1]

/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode() : val(0), next(nullptr) {}
 *     ListNode(int x) : val(x), next(nullptr) {}
 *     ListNode(int x, ListNode *next) : val(x), next(next) {}
 * };
 */
struct ListNode {
	int val;
	ListNode *next;
	ListNode()
		: val(0)
		, next(nullptr)
	{
	}
	ListNode(int x)
		: val(x)
		, next(nullptr)
	{
	}
	ListNode(int x, ListNode *next)
		: val(x)
		, next(next)
	{
	}
	// Since LeetCode's ListNode didn't have a head or a destructor,
	// I added this just to shut up AddressSanitizer in main().
	void naive_destructor()
	{
		ListNode *current = next;
		while (current != nullptr) {
			ListNode *next = current->next;
			delete current;
			current = next;
		}
	}
};

// My implementation #1
ListNode *addTwoNumbers1(ListNode *l1, ListNode *l2)
{
	ListNode li_base(0);
	ListNode *li_curr = &li_base;
	// map of "index" in linked list : amount to carry
	std::map<int, int> carry_ints {};
	int pos = 0;
	// Want to traverse till end, not till `->next == nullptr` (1 away from end)
	while (l1 != nullptr) {
		int node_total = l1->val + carry_ints[pos];
		if (l2 != nullptr) {
			node_total += l2->val;
		}

		if (l2 != nullptr) {
			l2 = l2->next;
		}

		// We carry a max of +1 to the next digit so no need to divide
		// to get the amount of "tens" to carry; 9+9+1 is still only carrying 1 to next pos
		if (node_total > 9) {
			carry_ints[pos + 1] = 1;
			li_curr->next = new ListNode(node_total - 10);
		}
		else {
			li_curr->next = new ListNode(node_total);
		}
		l1 = l1->next;
		li_curr = li_curr->next;
		pos++;
	}

	// In case l2 is longer than l1, iterate that
	while (l2 != nullptr) {
		int node_total = l2->val + carry_ints[pos];
		if (node_total > 9) {
			carry_ints[pos + 1] = 1;
			li_curr->next = new ListNode(node_total - 10);
		}
		else {
			li_curr->next = new ListNode(node_total);
		}
		li_curr = li_curr->next;
		l2 = l2->next;
		pos++;
	}

	// In case we _still_ have to carry 1 past
	// the end of the longest linked list
	if (carry_ints[pos] != 0) {
		li_curr->next = new ListNode(carry_ints[pos]);
		pos++;
	}
	return li_base.next;
}

// My implementation #2
// NOTE: l1 may be longer than l2, l2 may be longer than l1. Both loops need to be able to carry (because I did it in a
// dumb way)
ListNode *addTwoNumbers2(ListNode *l1, ListNode *l2)
{
	ListNode li_base(0);
	ListNode *li_curr = &li_base;
	// map of "index" in linked list : amount to carry
	int carry_next = 0;
	// Want to traverse till end, not till `->next == nullptr` (1 away from end)
	while (l1 != nullptr) {
		int node_total = l1->val + carry_next;
		if (l2 != nullptr) {
			node_total += l2->val;
		}

		if (l2 != nullptr) {
			l2 = l2->next;
		}

		// We carry a max of +1 to the next digit so no need to divide
		// to get the amount of "tens" to carry; 9+9+1 is still only carrying 1 to next pos
		if (node_total > 9) {
			carry_next = 1;
			li_curr->next = new ListNode(node_total - 10);
		}
		else {
			carry_next = 0;
			li_curr->next = new ListNode(node_total);
		}
		l1 = l1->next;
	li_curr = li_curr->next;
	}

	// In case l2 is longer than l1, iterate that.
	// This might still carry from the previous l1+l2
	while (l2 != nullptr) {
	int node_total = l2->val + carry_next;
	if (node_total > 9) {
		carry_next = 1;
		li_curr->next = new ListNode(node_total - 10);
	}
	else {
		carry_next = 0;
		li_curr->next = new ListNode(node_total);
	}
	li_curr = li_curr->next;
	l2 = l2->next;
	}

	// In case we _still_ have to carry 1 past
	// the end of the longest linked list
	if (carry_next != 0) {
		li_curr->next = new ListNode(carry_next);
	}
	return li_base.next;
}



#ifndef SNIPPET_ENTRYPOINT_SET
#define SNIPPET_ENTRYPOINT_SET
auto main() -> int
{
	ListNode input_1(2);
	input_1.next = new ListNode(4);
	input_1.next->next = new ListNode(3);

	ListNode input_2(5);
	input_2.next = new ListNode(6);
	input_2.next->next = new ListNode(4);

	ListNode *res_1 = addTwoNumbers2(&input_1, &input_2);

	std::cout << "expected result: 708" << '\n';
	std::cout << "actual result: " << res_1->val << res_1->next->val << res_1->next->next->val << '\n';
	input_1.naive_destructor();
	input_2.naive_destructor();
	res_1->naive_destructor();
	delete res_1;
	SNIPPET_LOCATION
}
#endif

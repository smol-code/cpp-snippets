#include "../util/snippet_location.hh"
#include <cstdint>
#include <limits>
#include <string>



// Leetcode:
// Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the value to go outside the
// signed 32-bit integer range [-231, 231 - 1], then return 0.
//
// Assume the environment does not allow you to store 64-bit integers (signed or unsigned).
//
// Example 1:
// Input: x = 123
// Output: 321
//
// Example 2:
// Input: x = -123
// Output: -321
//
// Example 3:
// Input: x = 120
// Output: 21

// In the case where, e.g., `2099999999` is passed in, `9999999902` would
// throw `std::out_of_range` if `std::stoul()` was actually constrained
// to uint32_t (unsigned long minimum spec). This code fails on actual
// 32 bit platforms ??? or I'm being dumb.
int reverse(int x)
{
	std::int32_t tmp = 0;
	std::string reversed;
	std::string prefix_minus = "";
	// 8573847412
	if (x == 0 || x < -2147483647) {
		return 0;
	}
	if (x < 0) {
		x = -x;
		prefix_minus = "-";
	}
	// Gets each digit in reverse order
	while (x != 0) {
		tmp = x % 10;
		reversed += std::to_string(tmp);
		x /= 10;
	}
	// Check if larger than signed int
	if (std::numeric_limits<std::int32_t>::max() < std::stoul(reversed)) {
		return 0;
	}
	else {
		return std::stoi(prefix_minus + reversed);
	}
}

#ifndef SNIPPET_ENTRYPOINT_SET
#define SNIPPET_ENTRYPOINT_SET
auto main() -> int
{
	std::int32_t input_1 { 123 };

	std::cout << "input: " << input_1 << '\n';
	std::cout << "expected result: 321" << '\n';
	std::cout << "actual result: " << reverse(input_1) << '\n';

	SNIPPET_LOCATION
}
#endif

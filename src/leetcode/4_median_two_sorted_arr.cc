#include "../util/snippet_location.hh"
#include <algorithm>
#include <vector>



// Leetcode:
// Given two sorted arrays nums1 and nums2 of size m and n respectively, return the median of the two sorted arrays.
//
// The overall run time complexity should be O(log (m+n)).
//
// Example 1:
// Input: nums1 = [1,3], nums2 = [2]
// Output: 2.00000
// Explanation: merged array = [1,2,3] and median is 2.
//
// Example 2:
// Input: nums1 = [1,2], nums2 = [3,4]
// Output: 2.50000
// Explanation: merged array = [1,2,3,4] and median is (2 + 3) / 2 = 2.5.

double findMedianSortedArrays1(std::vector<int> &nums1, std::vector<int> &nums2)
{
	std::vector<int> v {};
	v.reserve(nums1.size() + nums2.size());

	for (int i = 0; i != nums1.size(); i++) {
		v.emplace_back(nums1[i]);
	}
	for (int i = 0; i != nums2.size(); i++) {
		v.emplace_back(nums2[i]);
	}

	std::sort(v.begin(), v.end());
	// if odd, return the exact median.
	size_t v_n = v.size() / 2;
	std::nth_element(v.begin(), v.begin() + v_n, v.end());

	if (v.size() % 2) {
		return v[v_n];
	}

	// std::nth_element messes up order
	std::sort(v.begin(), v.end());

	// if even, v_n is the number after the "median", not before
	return (double)(v[v_n] + v[v_n - 1]) / 2;
}

double findMedianSortedArrays2(std::vector<int> &nums1, std::vector<int> &nums2)
{
	nums1.reserve(nums2.size());
	for (int i = 0; i != nums2.size(); i++) {
		nums1.emplace_back(nums2[i]);
	}

	// if odd, return the exact median.
	size_t v_n = nums1.size() / 2;
	std::nth_element(nums1.begin(), nums1.begin() + v_n, nums1.end());

	if (nums1.size() % 2) {
		return nums1[v_n];
	}

	// std::nth_element changes order and puts the median,
	// in the right place, so it doesn't need pre-sorting,
	// but sorting is needed here to get the "prior" number
	// if length is even
	std::sort(nums1.begin(), nums1.end());

	// if even, v_n is the number after the "median", not before
	return static_cast<double>(nums1[v_n] + nums1[v_n - 1]) / 2;
}



#ifndef SNIPPET_ENTRYPOINT_SET
#define SNIPPET_ENTRYPOINT_SET
auto main() -> int
{
	std::vector<int> input_1 { 1, 3 };
	std::vector<int> input_2 { 2 };

	std::cout << "input: {";
	for (const auto &num : input_1) {
		std::cout << num;
	}
	std::cout << "}" << '\n' << ", {";
	for (const auto &num : input_2) {
		std::cout << num;
	}
	std::cout << "}" << '\n';

	std::cout << "expected result: 2\n";

	std::cout << "actual result: " << findMedianSortedArrays2(input_1, input_2) << '\n';

	SNIPPET_LOCATION
}
#endif

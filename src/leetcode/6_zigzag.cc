#include "../util/snippet_location.hh"
#include <vector>



// Leetcode:
// The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: (you may want to
// display this pattern in a fixed font for better legibility)
//
// P   A   H   N
// A P L S I I G
// Y   I   R
//
// And then read line by line: "PAHNAPLSIIGYIR"
//
// Write the code that will take a string and make this conversion given a number of rows:
//
// string convert(string s, int numRows);
//
// Example 1:
// Input: s = "PAYPALISHIRING", numRows = 3
// Output: "PAHNAPLSIIGYIR"
//
// Example 2:
// Input: s = "PAYPALISHIRING", numRows = 4
// Output: "PINALSIGYAHRPI"
// Explanation:
// P     I    N
// A   L S  I G
// Y A   H R
// P     I
//
// Example 3:
// Input: s = "A", numRows = 1
// Output: "A"

std::string convert(std::string str, int num_rows)
{
	// Not splitting into rows
	if (num_rows < 2) {
		return str;
	}

	// Holds each row+col.
	// Could use a std::string instead of std::vector<char>
	std::vector<std::vector<char>> string_table(num_rows
		// ,std::vector<char>()
	);

	int curr_row = 0;
	// If not going to next row, it's going to previous row
	bool next_row = true;

	for (int i = 0; i != str.size(); i++) {
		string_table[curr_row].push_back(str[i]);
		if (curr_row == 0) {
			next_row = true;
		}
		else if (curr_row == num_rows - 1) {
			next_row = false;
		}

		if (next_row) {
			curr_row++;
		}
		else {
			curr_row--;
		}
	}

	std::string ret_str = "";
	for (int i = 0; i != num_rows; i++) {
		ret_str += std::string(string_table[i].begin(), string_table[i].end());
	}

	return ret_str;
}



#ifndef SNIPPET_ENTRYPOINT_SET
#define SNIPPET_ENTRYPOINT_SET
auto main() -> int
{
	std::string input_1 { "PAYPALISHIRING" };

	std::cout << "input: " << input_1 << '\n';
	std::cout << "expected result: PAHNAPLSIIGYIR" << '\n';
	std::cout << "actual result: " << convert(input_1, 3) << '\n';

	SNIPPET_LOCATION
}
#endif

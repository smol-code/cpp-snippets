#include "../util/snippet_location.hh"
#include <unordered_map>
#include <vector>



std::vector<int> twoSum1(std::vector<int> &nums, int target)
{
	std::unordered_map<int, int> traversed;
	for (int i = 0; i != nums.size(); i++) {
		int diff = target - nums[i];
		if (traversed.count(diff)) {
			return { traversed[diff], i };
		}
		traversed[nums[i]] = i;
	}
	return {};
}



#ifndef SNIPPET_ENTRYPOINT_SET
#define SNIPPET_ENTRYPOINT_SET
auto main() -> int
{
	// NOLINTBEGIN(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
	std::vector<int> vec_1 { 2, 7, 11, 15 };
	twoSum1(vec_1, 9);
	std::vector<int> vec_2 { 3, 2, 4 };
	twoSum1(vec_2, 6);
	std::vector<int> vec_3 { 3, 3 };
	twoSum1(vec_3, 6);
	SNIPPET_LOCATION
	// NOLINTEND(cppcoreguidelines-avoid-magic-numbers, readability-magic-numbers)
}
#endif

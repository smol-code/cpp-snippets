#include "../util/snippet_location.hh"
#include <algorithm>
#include <unordered_map>
#include <vector>



// Leetcode:
// Given a string s, find the length of the longest
// substring
// without repeating characters.
//
// Example 1:
// Input: s = "abcabcbb"
// Output: 3
// Explanation: The answer is "abc", with the length of 3.
//
// Example 2:
// Input: s = "bbbbb"
// Output: 1
// Explanation: The answer is "b", with the length of 1.
//
// Example 3:
// Input: s = "pwwkew"
// Output: 3
// Explanation: The answer is "wke", with the length of 3.
// Notice that the answer must be a substring, "pwke" is a subsequence and not a substring.

int lengthOfLongestSubstring1(std::string s)
{
	// Order unnecessary for result
	std::unordered_map<char, bool> best_sequence {};
	// Needs to be ordered as going to the "next" char
	// and finding a seen-before key requires it and any previous
	// keys removed from the map
	std::vector<char> curr_sequence {};
	curr_sequence.reserve(s.length());
	for (const auto &ch : s) {
		const auto pos_in_seq = std::find(curr_sequence.begin(), curr_sequence.end(), ch);
		// char not in substr sequence
		if (pos_in_seq == curr_sequence.end()) {
			curr_sequence.emplace_back(ch);
			continue;
		}

		if (curr_sequence.size() > best_sequence.size()) {
			best_sequence.clear();
			best_sequence.reserve(curr_sequence.size());
			for (const auto &curr_pair : curr_sequence) {
				best_sequence[curr_pair] = true;
			}
		}

		// char is in sequence, need to remove where it previously was
		// and everything before as substrings need to be contiguous
		// Need to "next" as the second pos in erase is not inclusive
		curr_sequence.erase(curr_sequence.begin(), std::next(pos_in_seq, 1));
		const auto index = pos_in_seq - curr_sequence.begin();
		curr_sequence.emplace_back(ch);
	}

	// Handle case where the "last" curr_sequence
	// wasn't inserted into best_sequence, because curr_sequence
	// was never interrupted (entire string is a substr)
	if (curr_sequence.size() > best_sequence.size()) {
		best_sequence.clear();
		best_sequence.reserve(curr_sequence.size());
		for (const auto &curr_pair : curr_sequence) {
			best_sequence[curr_pair] = true;
		}
	}
	return best_sequence.size();
}



#ifndef SNIPPET_ENTRYPOINT_SET
#define SNIPPET_ENTRYPOINT_SET
auto main() -> int
{
	std::string input { "abcabcbb" };
	std::cout << "input: " << input << '\n';
	std::cout << "expected result: 3 (abc)" << '\n';
	std::cout << "actual result: " << lengthOfLongestSubstring1(input) << '\n';
	SNIPPET_LOCATION
}
#endif

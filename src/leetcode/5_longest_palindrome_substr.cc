#include "../util/snippet_location.hh"
#include <algorithm>



// Leetcode:
// Given a string s, return the longest
// palindromic
// substring
// in s.
//
//
// Example 1:
// Input: s = "babad"
// Output: "bab"
// Explanation: "aba" is also a valid answer.
//
// Example 2:
// Input: s = "cbbd"
// Output: "bb"

int scan_from_centre(std::string s, int pos_left, int pos_right)
{
	// if not already completely at start or end,
	// and if the next outward chars are the same,
	// these next outward chars make up a valid palindrome
	while (pos_left >= 0 && pos_right < s.size() && s[pos_left] == s[pos_right]) {
		pos_left--;
		pos_right++;
	}
	return pos_right - pos_left - 1;
}

std::string longestPalindrome(std::string str)
{
	// if length of string is less than 1 return empty string
	if (str.length() < 2)
		return str;
	int start { 0 };
	int end { 0 };
	for (int i { 0 }; i < str.size(); i++) {
		// +1 for even length palindromes
		int len1 = scan_from_centre(str, i, i);
		int len2 = scan_from_centre(str, i, i + 1);
		int len = std::max(len1, len2);
		if (len > end - start) {
			start = i - ((len - 1) / 2);
			end = i + (len / 2);
		}
	}
	return str.substr(start, end - start + 1);
}



#ifndef SNIPPET_ENTRYPOINT_SET
#define SNIPPET_ENTRYPOINT_SET
auto main() -> int
{
	std::string input_1 { "babad" };

	std::cout << "input: " << input_1 << '\n';
	std::cout << "expected result: bab || aba" << '\n';
	std::cout << "actual result: " << longestPalindrome(input_1) << '\n';

	SNIPPET_LOCATION
}
#endif

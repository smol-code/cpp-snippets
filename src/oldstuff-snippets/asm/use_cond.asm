use_cond(std::array<int, 1000000ul>):
        push    rbp
        mov     rbp, rsp
        push    rbx
        sub     rsp, 40
        call    std::chrono::_V2::system_clock::now()
        mov     QWORD PTR [rbp-32], rax
        mov     DWORD PTR [rbp-20], 0
        jmp     .L1
.L4:
        mov     eax, DWORD PTR [rbp-20]
        cdqe
        mov     rsi, rax
        lea     rdi, [rbp+16]
        call    std::array<int, 1000000ul>::operator[](unsigned long)
        mov     ebx, DWORD PTR [rax]
        call    half_max()
        cmp     ebx, eax
        jg      .L3
        mov     eax, DWORD PTR [rbp-20]
        cdqe
        mov     rsi, rax
        lea     rdi, [rbp+16]
        call    std::array<int, 1000000ul>::operator[](unsigned long)
        mov     eax, DWORD PTR [rax]
        lea     ebx, [rax+rax]
        jmp     .L2
.L3:
        mov     ebx, 8
.L2:
        mov     eax, DWORD PTR [rbp-20]
        cdqe
        mov     rsi, rax
        lea     rdi, [rbp+16]
        call    std::array<int, 1000000ul>::operator[](unsigned long)
        mov     DWORD PTR [rax], ebx
        add     DWORD PTR [rbp-20], 1
.L1:
        cmp     DWORD PTR [rbp-20], 1000000
        jne     .L4
        call    std::chrono::_V2::system_clock::now()
        mov     QWORD PTR [rbp-40], rax
        lea     rdx, [rbp-32]
        lea     rax, [rbp-40]
        mov     rsi, rdx
        mov     rdi, rax
        call    std::common_type<std::chrono::duration<long, std::ratio<1l, 1000000000l> >, std::chrono::duration<long, std::ratio<1l, 1000000000l> > >::type std::chrono::operator-<std::chrono::_V2::system_clock, std::chrono::duration<long, std::ratio<1l, 1000000000l> >, std::chrono::duration<long, std::ratio<1l, 1000000000l> > >(std::chrono::time_point<std::chrono::_V2::system_clock, std::chrono::duration<long, std::ratio<1l, 1000000000l> > > const&, std::chrono::time_point<std::chrono::_V2::system_clock, std::chrono::duration<long, std::ratio<1l, 1000000000l> > > const&)
        mov     QWORD PTR [rbp-48], rax
        lea     rax, [rbp-48]
        mov     rdi, rax
        call    std::chrono::duration<long, std::ratio<1l, 1000000000l> >::count() const
        mov     rbx, QWORD PTR [rbp-8]
        leave
        ret
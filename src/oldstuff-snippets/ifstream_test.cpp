// NOLINTBEGIN

#include <iostream>
#include <string>
#include <fstream>
#include <filesystem>

int main()
{
	std::cout << "Enter input file name" << std::endl;

	std::string inFilename;
	std::getline(std::cin, inFilename);
	std::string currentWorkingDir = std::filesystem::current_path();
	std::cout << "Selected file: " << currentWorkingDir << "/" << inFilename << std::endl;

	std::ifstream inFileHandle;

	inFileHandle.open(inFilename);

	int aaaaaa;

	if (inFileHandle)
	{
		std::string str;
		while (std::getline(inFileHandle, str))
    	{
			std::cout << str << std::endl;
	    }
		inFileHandle.close();
	}

	else
	{
		std::cout << "Error reading file: " << inFilename << std::endl;
	}

	system("pause");
}

// NOLINTEND

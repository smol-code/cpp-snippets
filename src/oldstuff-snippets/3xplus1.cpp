// NOLINTBEGIN

#include <iostream>
#include <vector>
#include <array>

const int is_odd(int num)
{
	return num & 1;
}

const int manipulate(const int prev)
{
	if (is_odd(prev))
	{
		// TODO: Pre-emptively divide by two because 3x+1
		// will always be even
		return prev * 3 + 1;
	}
	else
	{
		return prev / 2;
	}
}

// -----------------------------------------------------------------------------
// TODO: Run over a list of source numbers and map each manipulation
// into a graph. Maybe as a "directed graph"
// TODO: Benchmark std::array at "some very large size", vs std::vector
// with inital and occasional .reserve(), vs std::vector without reserves
// -----------------------------------------------------------------------------
int main()
{
	const int call_limit = 100000;

	int num = 9663;
	// int num = 27;

	// int num = 8;
	// int num = 16;
	// int num = 32;
	// int num = 64;
	// int num = 256;
	// int num = 384;
	// int num = 512;
	// int num = 768;
	// int num = 1024;
	// int num = 2048;
	// int num = 4096;
	// int num = 8192;
	// int num = 16384;

	int call_count = 0;
	// std::array<int, call_limit + 1> manipulations{};
	// manipulations[0] = num;

	std::vector<int> manipulations;
	manipulations.push_back(num);
	manipulations.reserve(10000);

	while(num != 1 && call_count < call_limit) {
		num = manipulate(num);
		call_count++;
		manipulations.push_back(num);
		// manipulations[call_count] = num;
	}

	for (int mnum : manipulations) {
		if (mnum == 0) {
			break;
		}
		std::cout << mnum << '\n';
	}
	std::cout << call_count << '\n';
	std::cout << manipulations.size() << '\n';

	return 0;
}

// NOLINTEND

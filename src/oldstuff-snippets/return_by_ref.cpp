// NOLINTBEGIN

#include <iostream>

// Return void; get reference here and modify instead
void via_reference(int &inVar)
{
	inVar = inVar * 3;
}

// Return int
int via_return(int inVar)
{
	return inVar * 3;
}

int main() {

	// startInt can't be modified; just for clarity
	const int startInt = 161803;
	int somethingElse = startInt;

	std::cout << "Starting value is: " << startInt << std::endl;

	int returnInt = via_return(startInt);
	via_reference(somethingElse);

	std::cout << "Return number: " << returnInt << std::endl;
	std::cout << "Reference-modified-thingy: " << somethingElse << std::endl;

	std::cin.get();

	return 1;
}

// NOLINTEND

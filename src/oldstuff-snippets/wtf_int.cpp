// NOLINTBEGIN

#include <iostream>
#include <thread>
#include <mutex>
#include <vector>

std::mutex mtx_mtx;
unsigned long long main_num;

void rand_int()
{
	// Rand number between 0-99 for how much it'll loop
	int loop = std::rand() % 100;
	// See: https://stackoverflow.com/a/1597426
	int num;

	// Add numbers
	for (int i = 0; i < loop; ++i)
	{
		// Initialise without values, which basically defaults to some dumb number
		int x;
		int y;
		// Add them to num, which was also unititialised
		num += x + y;
	}

	// Invert to a positive if it was a negative
	if (num < 0)
	{
		num = -num;
	}

	// Here this adds the number within rand_int to the global main_num
	// but because mutexes aren't used, this can create race conditions
	// std::unique_lock<std::mutex> mtx(mtx_mtx);
	main_num += num;
	std::cout << "num: " << num << std::endl;
	std::cout << "main num: " << main_num << std::endl;
	// mtx_mtx.unlock();
}

void spawner()
{
	// thr_count is a random number between 0-99
	int thr_count = std::rand() % 100;

	// Same thing as in main
	std::vector<std::thread> threads;
	threads.reserve(thr_count);

	// Same thing as in main, but this time it runs the rand_int function on a thread
	for (int i = 0; i < thr_count; ++i)
	{
		threads.emplace_back(std::thread(rand_int));
	}

	for (int i = 0; i < threads.size(); ++i)
	{
		threads[i].join();
	}
}

int main()
{
	// Set up rand() so it actually gives pseud-random numbers
	std::srand((unsigned int)time(NULL));

	// thr_count is a random number between 0-99
	int thr_count = rand() % 100;

	// Vector (list) of threads
	std::vector<std::thread> threads;
	threads.reserve(thr_count);

	// Spawn number of threads based on thr_count, add to vector
	// this spawns "spawner" functions, which then spawn rand_int functions
	for (int i = 0; i < thr_count; ++i)
	{
		threads.emplace_back(std::thread(spawner));
	}

	// Join threads back together
	for (int i = 0; i < threads.size(); ++i)
	{
		threads[i].join();
	}

	// Print out
	std::cout << "final main num: " << main_num << std::endl;
}

// NOLINTEND

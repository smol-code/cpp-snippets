// NOLINTBEGIN

#include <vector>
#include <algorithm>
#include <iostream>

// Based completely off of https://www.youtube.com/watch?v=f2xi3c1S95M

int get_min_steps_tab(int n) {
	std::vector<int> table(n + 1, n);

	table[1] = 0;

	for(int i = 1; i < n; i++) {
		table[i + 1] = std::min(table[i + 1], table[i] + 1);
		if (i * 2 <= n) {
			table[i * 2] = std::min(table[i] + 1, table[i * 2]);
		}
		if (i * 3 <= n) {
			table[i * 3] = std::min(table[i] + 1, table[i * 3]);
		}
	}
	return table[n];
}

int main() {
	std::cout << get_min_steps_tab(109348) << std::endl;
	std::cout << "hello" << std::endl;
}

// NOLINTEND

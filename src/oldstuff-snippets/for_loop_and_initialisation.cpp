// NOLINTBEGIN

#include <iostream>

int main()
{
	int numTestA = 8;
	for (int i = 0; i < numTestA; i++)
	{
		std::cout << "loopA " << i << std::endl;
	}

	int numTestB{8};
	for (int i{0}; i != numTestB; ++i)
	{
		std::cout << "loopB " << i << std::endl;
	}

	return 0;
}

// NOLINTEND

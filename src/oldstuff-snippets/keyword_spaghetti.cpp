// NOLINTBEGIN

#include <iostream>

class mon_coeur {
public:
    [[nodiscard]] constexpr virtual int asdf() final {
        return 1;
    }
};

int main() {
	float a = 26.4-25;

    mon_coeur haato;
    const auto b = haato.asdf();

    std::cout << a << b << '\n';

}

// NOLINTEND

// NOLINTBEGIN

#include <array>
#include <vector>
#include <iostream>
#include <chrono>

constexpr int half_max() {
	return RAND_MAX/2;
}

const int arr_size = 1000000;

constexpr std::array<int, arr_size> prand_fill_arr() {
	std::array<int, arr_size> ab{};
	for (int i = 0; i != arr_size; ++i) {
		// ab[i] = std::atoi(__TIME__) + i;
		ab[i] = i;
	}
	return ab;
}

int use_if(std::array<int, arr_size> arr) {
	auto start = std::chrono::system_clock::now();
	for (int i = 0; i != arr_size; ++i) {
		if (arr[i] > half_max()) {
			arr[i] = 8;
		}
		else {
			arr[i] = arr[i] * 2;
		}
	}
	auto end = std::chrono::system_clock::now();
	auto elapsed = end - start;
	// std::cout << "if loop: " << elapsed.count() << '\n';
	return elapsed.count();
}


int use_cond(std::array<int, arr_size> arr) {
	auto start = std::chrono::system_clock::now();
	for (int i = 0; i != arr_size; ++i) {
		arr[i] = arr[i] > half_max() ? 8 : arr[i] * 2;
		// arr[i] > half_max() ? arr[i] = 8 : 0;
	}
	auto end = std::chrono::system_clock::now();
	auto elapsed = end - start;
	// std::cout << "cond loop: " << elapsed.count() << '\n';
	return elapsed.count();
}

int main() {
	std::array<int, arr_size> a1 {prand_fill_arr()};

	int elapsed_inner_cond = 0;
	int elapsed_inner_if = 0;
	
	auto start_cond = std::chrono::system_clock::now();
	for (int i = 0; i != 100; ++i) {
		elapsed_inner_cond += use_cond(a1);
	}
	auto end_cond = std::chrono::system_clock::now();
	auto elapsed_cond = end_cond - start_cond;
	std::cout << "cond elapsed: " << elapsed_cond.count() << '\n';

	auto start_if = std::chrono::system_clock::now();
	for (int i = 0; i != 100; ++i) {
		elapsed_inner_if += use_if(a1);
	}
	auto end_if = std::chrono::system_clock::now();
	auto elapsed_if = end_if - start_if;
	std::cout << "if elapsed:   " << elapsed_if.count() << '\n';

	std::cout << "cond elapsed inner: " << elapsed_inner_cond << '\n';
	std::cout << "if elapsed inner:   " << elapsed_inner_if << '\n';
	return 0;
}

// NOLINTEND

#!/bin/bash

# Downloads and builds Google Test as needed by Google Benchmark
# https://github.com/google/benchmark?tab=readme-ov-file#installation

# cmake -DCMAKE_BUILD_TYPE=Debug -DBENCHMARK_DOWNLOAD_DEPENDENCIES=on -S ./ -B ./build || exit
cmake -DCMAKE_BUILD_TYPE=Debug -S ./ -B ./build || exit
cmake --build ./build --parallel 16 || exit
cd ./build || exit
echo
echo "----- built successfully -----"
echo
echo "---------- running tests ----------"
echo
./all_tests
echo
echo "---------- running benchmarks ----------"
echo
./all_benchmarks

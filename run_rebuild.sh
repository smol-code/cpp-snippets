#!/bin/bash

# Rebuild the project
# Allows arg to be passed to correspond to a standalone snippet being built,
# otherwise benchmarks and tests are built.

	# If user provided an arg, run the corresponding built snippet
if [ ! -z "$1" ]; then
	cmake --build ./build --parallel 16 --target "$1" || exit
	cd ./build || exit
	# echo "----- built successfully (snippet: $1) -----"
	echo "----- built successfully (snippet) -----"
	echo
	./"$1"
else
	cmake --build ./build --parallel 16 --target "all_tests" "all_benchmarks" || exit
	cd ./build || exit
	echo "----- built successfully (all tests and benchmarks) -----"
	echo
	echo "---------- running tests ----------"
	echo
	./all_tests
	echo
	echo "---------- running benchmarks ----------"
	echo
	./all_benchmarks
fi


#include <cstring>
#include <iostream>



auto main(int argc, char *argv[]) -> int
{
	// Don't want to spam console on ./run_rebuild.sh, but still want to run exe
	if (argc == 2 && std::strcmp(argv[1], "--no-spam") == 0) {
		std::cout << "success :3" << '\n';
		return 0;
	}
	std::cout << '\n';
	std::cout << "- This is a placeholder executable." << '\n';
	std::cout << "- The snippets are built from their respective folders with prefix `exe_{folder}_`." << '\n';
	std::cout << "- `src/util` is excluded from being built as a snippet; it's where libraries are linked and any of "
				 "my own \"lib\" code goes"
			  << '\n';
	std::cout << "- NOTE: Any new folders created in `src/` need to be accounted for in `CMakeLists.txt`" << '\n';
	std::cout << '\n';
	std::cout << '\n';
	return 0;
};
